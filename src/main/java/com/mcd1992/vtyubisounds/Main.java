package com.mcd1992.vtyubisounds;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.MemorySection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
    private static Main pluginInstance;
    private static Logger log;
    private static YamlConfiguration config;

    public HashMap<UUID, Cooldown> cooldownMap;
    public HashMap<String, Sound> soundsMap;
    public double currentPlayerCount;
    public double maxPlayerCount;
    double currentPlayerCapacity;

    public String helpListString;

    public boolean configEnabled;
    public boolean configRequirePrefix;

    public String configGlobalHidePrefix;
    public String configGlobalSoundSuffix;
    public float  configGlobalVolume;
    public double configGlobalMinCooldown;
    public double configGlobalMaxCooldown;

    public String configLocalHidePrefix;
    public String configLocalSoundSuffix;
    public float  configLocalVolume;
    public double configLocalMinCooldown;
    public double configLocalMaxCooldown;

    @Override
    public void onEnable() {
        pluginInstance = this;
        log = getLogger();
        currentPlayerCount = 0;

        saveDefaultConfig();
        loadConfig();
        getServer().getPluginManager().registerEvents(new Listeners(this), this);
        PluginCommand cmd = getCommand("vtyubisounds");
        if (cmd instanceof PluginCommand) {
            Commands c = new Commands(this);
            cmd.setExecutor(c);
            cmd.setTabCompleter(c);
        }
    }

    @Override
    public void onDisable() {
        pluginInstance = null;
    }

    @Override
    public YamlConfiguration getConfig() {
        return config;
    }

    public Main getInstance() {
        return pluginInstance;
    }

    public void loadConfig() {
        cooldownMap = new HashMap<UUID, Cooldown>();
        soundsMap = new HashMap<String, Sound>();
        maxPlayerCount = this.getServer().getMaxPlayers();
        helpListString = "";

        File configFolder = getDataFolder();
        File configFile = new File(configFolder, "config.yml");
        config = new YamlConfiguration();
        try {
            // Either SnakeYAML or bukkit/spigot's YAML doesn't understand what quotes are...
            // Even if you put a . in quotes it will still be parsed as nested keys. I love java devs.
            config.options().pathSeparator(';');
            config.loadFromString(Files.readString(Path.of(configFile.getPath())));
        } catch(Exception e) {
            log.severe(String.format("Failed to load config: %s", e.toString()));
            return;
        }

        // Generate default config if configVersion mismatch
        if (!config.isSet("configVersion") || config.getDouble("configVersion") < 2) {
            config.addDefault("configVersion", 2);
            config.addDefault("logLevel", "INFO");

            config.addDefault("enabled", true);
            config.addDefault("requirePrefix", false);

            config.addDefault("globalHidePrefix", ",");
            config.addDefault("globalSoundSuffix", null);
            config.addDefault("globalVolume", 1.0f);
            config.addDefault("globalMinCooldown", 10.0);
            config.addDefault("globalMaxCooldown", 10.0);

            config.addDefault("localHidePrefix", "~");
            config.addDefault("localSoundSuffix", null);
            config.addDefault("localVolume", 6.0);
            config.addDefault("localMinCooldown", null);
            config.addDefault("localMaxCooldown", null);

            LinkedHashMap exampleSounds = new LinkedHashMap();
            exampleSounds.put("minecraft:entity.creeper.primed", "hiss");
            LinkedHashMap exampleOptions = new LinkedHashMap();
            ArrayList<String> exampleTriggers = new ArrayList();
            exampleTriggers.add("/.*rustybullethole.*");
            exampleOptions.put("triggers", exampleTriggers);
            exampleOptions.put("pitch", 0.5);
            exampleSounds.put("minecraft:entity.dragon_fireball.explode", exampleOptions);
            config.addDefault("sounds", exampleSounds);

            config.options().header("""
                Trigger words, without a / prefix, will be parsed with String.startsWith
                The target sound must be in namespace:plugin.soundname format
                If your trigger word contains a semicolon ; it must be escaped

                Trigger words beginning with '/' are parsed as regular expression
                See: https://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html

                Example Sounds
                sounds:
                  minecraft:entity.creeper.primed: hiss
                  minecraft:entity.dragon_fireball.explode:
                    triggers:
                    - /.*boom.*
                    pitch: 0.5
            """).copyDefaults(true);
            try {
                config.save(configFile);
                log.info("Created default config at: " + configFile.toString());
            } catch (IOException err) {
                log.warning(err.toString());
            }
        }
        //log.info(String.format("Loaded config:\n%s\n\n", config.saveToString()));

        log.setLevel(Level.parse(config.getString("logLevel", "INFO")));
        configEnabled = config.getBoolean("enabled", true);
        configRequirePrefix = config.getBoolean("requirePrefix", false);

        configGlobalHidePrefix = config.getString("globalHidePrefix", ",");
        configGlobalSoundSuffix = config.getString("globalSoundSuffix", null);
        configGlobalVolume = (float)config.getDouble("globalVolume", 1.0f);
        configGlobalMinCooldown = config.getDouble("globalMinCooldown", 10.0);
        configGlobalMaxCooldown = config.getDouble("globalMaxCooldown", 10.0);

        configLocalHidePrefix = config.getString("localHidePrefix", "~");
        configLocalSoundSuffix = config.getString("localSoundSuffix", null);
        configLocalVolume = (float)config.getDouble("localVolume", 6.0);
        configLocalMinCooldown = config.getDouble("localMinCooldown");
        configLocalMaxCooldown = config.getDouble("localMaxCooldown");

        Map<String,Object> sounds;
        MemorySection cfgSounds = (MemorySection)config.get("sounds", null);
        if (cfgSounds instanceof MemorySection) {
            sounds = cfgSounds.getValues(false);
        } else {
            log.warning("No sounds found in config");
            return;
        }

        for (String soundName : sounds.keySet()) {
            var triggerInfo = sounds.get(soundName);
            Sound sound = null;

            try {
                if (triggerInfo instanceof String triggerStr) { // register simple sound
                    sound = new Sound(soundName, triggerStr);
                } else if (triggerInfo instanceof MemorySection options) {
                    sound = new Sound(soundName, options.getValues(false));
                } else {
                    log.warning(String.format(
                        "Invalid options for sound %s: [%s] %s",
                        soundName, triggerInfo.getClass().toString(),
                        triggerInfo.toString())
                    );
                }
                helpListString = helpListString.trim() + " ";
                helpListString += (sound != null ? sound.helpText() : "");
                helpListString = helpListString.trim();
                log.fine("added sound: " + soundName);
            } catch(Exception e) {
                log.warning("Failed to add sound, invalid regex or options? " + soundName);
                log.warning(e.toString());
                continue;
            }

            if (sound != null && soundsMap.putIfAbsent(soundName, sound) != null) {
                log.warning("Duplicate sound in config: " + soundName);
            }
        }
    }
}
