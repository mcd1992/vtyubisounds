package com.mcd1992.vtyubisounds;

import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.bukkit.SoundCategory;

public class Sound {
    public String targetSound;
    public ArrayList<Trigger> triggers;
    public SoundCategory category = SoundCategory.VOICE;
    public float volume = 1.0f;
    public float pitch = 1.0f;

    /* TODO:
    public String  helpListString;
    public boolean enabled;
    public boolean requirePrefix;

    public String  globalSoundSuffix;
    public float   globalVolume;
    public double  globalCooldownOffset;

    public String  localSoundSuffix;
    public float   localVolume;
    public double  localCooldownOffset;
    */

    Sound(String soundName, String triggerString) {
        targetSound = soundName;
        triggers = new ArrayList<Trigger>();
        triggers.add(new Trigger(triggerString));
    }

    Sound(String soundName, Map options) {
        targetSound = soundName;
        triggers = new ArrayList<Trigger>();

        ArrayList<String> trigStrings = (ArrayList<String>)options.getOrDefault("triggers", new ArrayList());
        for (String t : trigStrings) {
            triggers.add(new Trigger(t));
        }
        category = SoundCategory.valueOf((String)options.getOrDefault("category", "VOICE"));
        //volume = (float)options.getOrDefault("volume", 1.0f);
        //pitch = (float)options.getOrDefault("pitch", 1.0f);
    }

    public boolean match(String msg) {
        for (Trigger trigger : triggers) {
            if (trigger.match(msg)) return true;
        }
        return false;
    }

    public String helpText() {
        String helpText = "";
        for (Trigger trigger : triggers) {
            helpText += trigger.triggerString + " ";
        }
        return helpText.trim();
    }
}

class Trigger {
    public boolean isRegex;
    public Pattern regexPattern;
    public String  triggerString;

    Trigger(String trigger) {
        isRegex = false;
        triggerString = trigger.toLowerCase().replaceAll("[^\\x20-\\x7E]", "");
        if (trigger.charAt(0) == '/') {
            isRegex = true;
            triggerString = triggerString.replaceAll("[^a-zA-Z0-9]", "");
            regexPattern = Pattern.compile(trigger.substring(1));
        }
    }

    public boolean match(String msg) {
        if (isRegex) {
            Matcher matcher = regexPattern.matcher(msg);
            return matcher.matches();
        } else {
            return msg.startsWith(triggerString);
        }
    }
}