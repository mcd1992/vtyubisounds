package com.mcd1992.vtyubisounds;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

public class Commands implements CommandExecutor, TabCompleter {
    private static Main pluginInstance;
    private static Logger log;

    private static ArrayList tabcomplete_empty;
    private static List tabcomplete_subcommands;

    Commands(Main instance) {
        pluginInstance = instance;
        log = instance.getLogger();
        tabcomplete_empty = new ArrayList<String>();
        tabcomplete_subcommands = Arrays.asList("list", "reload");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length < 1) {
            return false;
        }
        boolean isPlayer = sender instanceof Player;

        String output = "";
        switch (args[0]) {
            case "list" -> {
                output = "Sounds: " + pluginInstance.helpListString;
            }
            case "reload" -> {
                if (sender.isOp()) {
                    pluginInstance.loadConfig();
                    output = "Configuration reloaded.";
                } else {
                    // TODO: luckperms
                    output = "Console or OP only.";
                }
            }
            case "debug" -> {
                if (!isPlayer && sender.isOp()) { // console only
                    log.warning(String.format("config: \n%s\n", pluginInstance.getConfig().saveToString()));
                    log.warning(String.format("cooldownMap: %s\n", pluginInstance.cooldownMap.toString()));
                    log.warning(String.format("soundsMap: %s\n", pluginInstance.soundsMap.toString()));
                } else {
                    return false;
                }
            }
            default -> {
                return false;
            }
        }

        if (isPlayer) {
            Player player = (Player)sender;
            player.sendMessage(String.format("%s[%s] %s%s",
                ChatColor.AQUA, pluginInstance.getName(), output, ChatColor.RESET
            ));
        } else {
            sender.sendMessage(output);
            //log.info(output);
        }

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (args.length < 2) {
            return tabcomplete_subcommands;
        }
        return tabcomplete_empty;
    }
}
